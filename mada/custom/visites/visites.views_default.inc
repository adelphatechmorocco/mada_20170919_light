<?php
/**
 * @file
 * visites.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function visites_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'visites';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sponsor_counter';
  $view->human_name = 'Visites';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Visites';
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Champ: [cvsponsor] Table sponsor_counter: Sponsor */
  $handler->display->display_options['fields']['sponsor']['id'] = 'sponsor';
  $handler->display->display_options['fields']['sponsor']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['sponsor']['field'] = 'sponsor';
  /* Champ: [cvsponsor] Table sponsor_counter: Daycount */
  $handler->display->display_options['fields']['daycount']['id'] = 'daycount';
  $handler->display->display_options['fields']['daycount']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['daycount']['field'] = 'daycount';
  /* Champ: [cvsponsor] Table sponsor_counter: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  /* Champ: [cvsponsor] Table sponsor_counter: Sponsor */
  $handler->display->display_options['fields']['sponsor_1']['id'] = 'sponsor_1';
  $handler->display->display_options['fields']['sponsor_1']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['sponsor_1']['field'] = 'sponsor';
  /* Champ: [cvsponsor] Table sponsor_counter: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['label'] = 'Horodatage';
  /* Champ: [cvsponsor] Table sponsor_counter: Totalcount */
  $handler->display->display_options['fields']['totalcount']['id'] = 'totalcount';
  $handler->display->display_options['fields']['totalcount']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['totalcount']['field'] = 'totalcount';
  /* Champ: Global: Contextual Links */
  $handler->display->display_options['fields']['contextual_links']['id'] = 'contextual_links';
  $handler->display->display_options['fields']['contextual_links']['table'] = 'views';
  $handler->display->display_options['fields']['contextual_links']['field'] = 'contextual_links';
  /* Champ: Global: Math expression */
  $handler->display->display_options['fields']['expression']['id'] = 'expression';
  $handler->display->display_options['fields']['expression']['table'] = 'views';
  $handler->display->display_options['fields']['expression']['field'] = 'expression';
  /* Champ: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  /* Filter criterion: [cvsponsor] Table sponsor_counter: Sponsor */
  $handler->display->display_options['filters']['sponsor']['id'] = 'sponsor';
  $handler->display->display_options['filters']['sponsor']['table'] = 'sponsor_counter';
  $handler->display->display_options['filters']['sponsor']['field'] = 'sponsor';
  $handler->display->display_options['filters']['sponsor']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sponsor']['expose']['operator_id'] = 'sponsor_op';
  $handler->display->display_options['filters']['sponsor']['expose']['label'] = 'Matricule';
  $handler->display->display_options['filters']['sponsor']['expose']['operator'] = 'sponsor_op';
  $handler->display->display_options['filters']['sponsor']['expose']['identifier'] = 'sponsor';
  $handler->display->display_options['filters']['sponsor']['expose']['required'] = 0;
  $handler->display->display_options['filters']['sponsor']['expose']['multiple'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Champ: [cvsponsor] Table sponsor_counter: Sponsor */
  $handler->display->display_options['fields']['sponsor']['id'] = 'sponsor';
  $handler->display->display_options['fields']['sponsor']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['sponsor']['field'] = 'sponsor';
  $handler->display->display_options['fields']['sponsor']['label'] = 'Matricule';
  $handler->display->display_options['fields']['sponsor']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['external'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['sponsor']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['sponsor']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['sponsor']['alter']['html'] = 0;
  $handler->display->display_options['fields']['sponsor']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['sponsor']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['sponsor']['hide_empty'] = 0;
  $handler->display->display_options['fields']['sponsor']['empty_zero'] = 0;
  $handler->display->display_options['fields']['sponsor']['hide_alter_empty'] = 1;
  /* Champ: [cvsponsor] Table sponsor_counter: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'PAGE';
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['nid']['separator'] = '';
  $handler->display->display_options['fields']['nid']['format_plural'] = 0;
  /* Champ: [cvsponsor] Table sponsor_counter: Totalcount */
  $handler->display->display_options['fields']['totalcount']['id'] = 'totalcount';
  $handler->display->display_options['fields']['totalcount']['table'] = 'sponsor_counter';
  $handler->display->display_options['fields']['totalcount']['field'] = 'totalcount';
  $handler->display->display_options['fields']['totalcount']['label'] = 'Accès';
  $handler->display->display_options['fields']['totalcount']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['external'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['totalcount']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['totalcount']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['totalcount']['alter']['html'] = 0;
  $handler->display->display_options['fields']['totalcount']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['totalcount']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['totalcount']['hide_empty'] = 0;
  $handler->display->display_options['fields']['totalcount']['empty_zero'] = 0;
  $handler->display->display_options['fields']['totalcount']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['totalcount']['format_plural'] = 0;
  $handler->display->display_options['path'] = 'admin/visites';
  $translatables['visites'] = array(
    t('Master'),
    t('Visites'),
    t('more'),
    t('Apply'),
    t('Réinitialiser'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Sponsor'),
    t('Daycount'),
    t('.'),
    t(','),
    t('Nid'),
    t('Horodatage'),
    t('Totalcount'),
    t('Contextual Links'),
    t('Math expression'),
    t('View result counter'),
    t('Matricule'),
    t('Page'),
    t('PAGE'),
    t('Accès'),
  );
  $export['visites'] = $view;

  return $export;
}
