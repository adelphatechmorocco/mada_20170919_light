<?php
/**
 * @file
 * visites.features.inc
 */

/**
 * Implements hook_views_api().
 */
function visites_views_api() {
  return array("version" => "3.0");
}
