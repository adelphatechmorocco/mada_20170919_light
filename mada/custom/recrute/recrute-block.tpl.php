<div class="bloc_offre">
    <h3><?php print('Dernières offres'); ?></h3>
    <ul class="city_offre">

	<?php
	$libile_offre = 'offres';
	if (!$node_count_rabat['count'] or $node_count_rabat['count'] == 1)
	    $libile_offre = 'offre';
	else
	    $libile_offre = 'offres';
	?>
	<li><?php print l(t('rabat') . '<span>' . $node_count_rabat['count'] .' '. t($libile_offre).'</span>', 'offre-emploi' , array('query' => array('field_ville_tid' => $node_count_rabat['id_ville']),'html' => TRUE)); ?></li>
	<?php
	if (!$node_count_kenitra['count'] or $node_count_kenitra['count'] == 1)
	    $libile_offre = 'offre';
	else
	    $libile_offre = 'offres';
	?>
	<li><?php print l(t('kénitra') . '<span>' . $node_count_kenitra['count'] .' '. t($libile_offre).'</span>','offre-emploi' , array('query' => array('field_ville_tid' => $node_count_kenitra['id_ville']),'html' => TRUE)); ?></li>
	<?php
	if (!$node_count_fes['count'] or $node_count_fes['count'] == 1)
	    $libile_offre = 'offre';
	else
	    $libile_offre = 'offres';
	?>
	<li><?php print l(t('fes') . '<span>' . $node_count_fes['count'] .' '.t($libile_offre).'</span>', 'offre-emploi' , array('query' => array('field_ville_tid' => $node_count_fes['id_ville']),'html' => TRUE)); ?></li>
	<?php
	if (!$node_count_agadir['count'] or $node_count_agadir['count'] == 1)
	    $libile_offre = 'offre';
	else
	    $libile_offre = 'offres';
	?>
	<li><?php print l(t('agadir') . '<span>' . $node_count_agadir['count'] .' '. t($libile_offre).'</span>', 'offre-emploi' , array('query' => array('field_ville_tid' => $node_count_agadir['id_ville']),'html' => TRUE)); ?></li>
    </ul>
    <div class="clear"></div>
	<?php
//$bloqueServicios = module_invoke('views', 'block_view', 'metier-block_metier_home');
//print render($bloqueServicios);
	?>
    <ul class="type_offre">         
    <?php
    for ($i = 1; $i <= $nbr_metier; $i++) {
		if($metier_label[$i]){
			echo "<li > ";
			echo l($metier_label[$i], $metier_lien[$i]);
			echo "</li>";
		}
	}
    ?>
    </ul>
    <div class="all_offre">
	<?php print l(t('Toutes les offres'), 'offre-emploi'); ?>
    </div>
</div>