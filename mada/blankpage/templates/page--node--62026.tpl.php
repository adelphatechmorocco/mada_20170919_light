 
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]>
<html>
<!<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>120 Métiers</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico"> 
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
</head>

<body class="login">
<div id="wrapper">
    <div id="content">
        <div class="box-metier">
            <div class="header">
                <a href="<?=base_path()?>#avenir" class="btn-back">Retour a l'accueil</a>
                <h2>Nos Métiers</h2>
                <p>
                    Webhelp, c'est plus de 120 métiers
                </p>
            </div>
          <div class="slider-videos">
                    <ul class="list-videos">
                        <li>
                <div class="pushs">
                

                    <div class="push"> 
                                    <img src="http://img.youtube.com/vi/cfAFjGQTINQ/hqdefault.jpg" alt="">
                                    <a href="#" class="play video-link" data-video-id="y-cfAFjGQTINQ" data-video-width="1100" data-video-height="650"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                                    <div class="info">
                                        <h2>El Mehdi</h2>
                                        <p>Conseiller Commercial</p>
                                    </div>
                                </div> <!-- push -->

                    <div class="push">
                                    <img src="http://img.youtube.com/vi/syec3_8I2CE/hqdefault.jpg" alt="">
                                    <a href="#" class="play video-link" data-video-id="y-syec3_8I2CE" data-video-width="1100" data-video-height="650"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                                    <div class="info">
                                        <h2>Fall</h2>
                                        <p>Superviseur </p>
                                    </div>
                                </div> <!-- push -->


                    <div class="push">
                                    <img src="http://img.youtube.com/vi/JSkrOmo-7UY/hqdefault.jpg" alt="">
                                    <a href="#" class="play video-link" data-video-id="y-JSkrOmo-7UY" data-video-width="1100" data-video-height="650"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                                    <div class="info">
                                        <h2>Mohammed</h2>
                                        <p>Formateur</p>
                                    </div>
                                </div> <!-- push -->

                    <div class="push">
                                    <img src="http://img.youtube.com/vi/KGnaOgDbvow/hqdefault.jpg" alt="">
                                    <a href="#" class="play video-link" data-video-id="y-KGnaOgDbvow" data-video-width="1100" data-video-height="650"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                                    <div class="info">
                                        <h2>Sarah</h2>
                                        <p>Responsable d’Activité</p>
                                    </div>
                                </div> <!-- push -->

                    <div class="push">
                        <img src="http://img.youtube.com/vi/uyj53dEj6YI/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="uyj53dEj6YI" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Alexandre</h2>
                            <p>Conseiller client</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/uaIPG9vm8RY/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="uaIPG9vm8RY" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Safia</h2>
                            <p>Conseillère client</p>
                        </div>
                    </div> <!-- push -->

                    <div class="push">
                        <img src="http://img.youtube.com/vi/qAZdsiEuAGo/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="qAZdsiEuAGo" data-video-width="1100" data-video-height="650"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Dora</h2>
                            <p>Formatrice</p>
                        </div>
                    </div> <!-- push -->

                    <div class="push">
                        <img src="http://img.youtube.com/vi/Szz_7fMAdXQ/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="Szz_7fMAdXQ" data-video-width="1100" data-video-height="650"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Jean-Baptiste</h2>
                            <p>Conseiller client</p>
                        </div>
                    </div> <!-- push -->
                    <div class="push">
                        <img src="http://img.youtube.com/vi/lUHED1la6Yk/hqdefault.jpg" alt="">
                        <a href="#" class="play video-link" data-video-id="lUHED1la6Yk" data-video-width="1100" data-video-height="650"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-metier.png" alt=""></a>
                        <div class="info">
                            <h2>Kaoutar</h2>
                            <p>Chef de projet</p>
                        </div>
                    </div> <!-- push -->
                    
                </div> <!-- pushs -->  
</li>				</ul>
                </div><!-- slider-videos -->
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/videoLightning.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js?v=3"></script>
</body>

</html>
