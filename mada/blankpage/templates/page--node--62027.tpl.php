<?php if ( $user->uid ) { ?>
<div class="box-login" id="box-login">
    <form action="<?=base_path()?>?q=user" method="POST" id="user-login">
	    <input type="hidden" name="form_build_id" value="form-1l6PtUZBDfn01DA1yeq8qf6yHk9vxHbPpABnKOE-d3k">
        <input type="hidden" name="form_id" value="user_login">
        <input type="hidden" name="lastcookie" value="">
        <div class="caption">
            <h1> webhelp
            <a href="<?=base_path()?>">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo-login.jpg" alt="">
            </a>
        </h1>
            <h2>Espace membre</h2>
        </div>
        <div class="content-form btns-user">
            <h2>Bienvenue 
                            <?php
                                $author = user_load($user->uid);
                                print $author->field_prenom[und][0][value];
                            ?> </h2>    
            <a href="<?=base_path()?>user/logout" class="link">Déconnexion</a>
        </div>
    </form>
</div>
<?php }else{ ?>
<div class="box-login" id="box-login">

    <div class="caption">
        <h1> webhelp
        <a href="<?=base_path()?>">
            <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo-login.jpg" alt="">
        </a>
    </h1>
        <h2>Espace membre</h2>
    </div>
    <div class="identif-left">
        <div class="connecte-toi">
            <p>Connectez-vous
                <br/> avec votre réseau social</p>
            <ul class="social-media">
                            <li><a href="<?=base_path()?>user/simple-fb-connect"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?=base_path()?>linkedin/login/0"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<!--                             <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li> -->
                        </ul>
        </div>
    </div>
    <form action="<?=base_path()?>?q=user" method="POST" id="user-login">
	    <input type="hidden" name="form_build_id" value="form-1l6PtUZBDfn01DA1yeq8qf6yHk9vxHbPpABnKOE-d3k">
        <input type="hidden" name="form_id" value="user_login">
        <input type="hidden" name="lastcookie" value="">
        <div class="content-form">
            <div class="formline content">
                <input required type="text" id="identifient" name="name"/>
                <label for="identifient">Identifiant</label> 
            </div>
            <div class="formline content">
                <input required type="password" id="motdepasse" name="pass"/>
                <label for="motdepasse">Mot de passe</label>
                <p class="forget"> 
                    <a href="<?=base_path()?>user/password">Mot de passe oublié ?</a>
                </p>
            </div>
            <div class="formline submit">
                <input type="hidden" name="form_id" id="user-login" value="user_login"  />
                <button type="submit">Connexion</button>
            </div>
            <a href="<?=base_path()?>user/register" class="link">Créer un compte</a>
        </div>
    </form>
</div>
<?php } ?>

