<?php
$query = new EntityFieldQuery;
$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'callback')
  ->propertyCondition('status', 1) 
  ->fieldCondition('field_status', 'value', 0);
$results = $query->execute();
print " ";
foreach ($results['node'] as $n) {
	$node=node_load($n->nid);
	$diff = round(abs(time() - $node->changed) / 60,2);
	if($diff>0){
		$usersUpdated=$field_gsm=array();
		
		$roleId = 7;

		$query = 'SELECT ur.uid
		    FROM {users_roles} AS ur
		    WHERE ur.rid = :rid';
		$result = db_query($query, array(':rid' => $roleId));
		$uids = $result->fetchCol();
		$usrs = "";
		foreach ($uids as $u) { $usrs.=$u.','; }
		
		//Select user to update
		$query = 'SELECT uid, name FROM wh_users AS u INNER JOIN wh_field_data_field_updated AS up ON u.uid = up.entity_id WHERE u.uid IN ('.rtrim($usrs,',').') ORDER BY up.field_updated_value ASC';
		$result = db_query($query);

		foreach ($result as $k => $v) { $usersUpdated[]=$v->uid; }

		if (!empty($usersUpdated)) {
		    $user = user_load($usersUpdated[0]);
		    $node->uid = $usersUpdated[0];
		    print $node->nid."-NEW".$usersUpdated[0]."-OLD".$node->field_uid_reference["und"][0][target_id]."-".$kdiff."<br />"; 
		    $node->field_uid_reference = array(LANGUAGE_NONE => array(array('uid' => (int)$usersUpdated[0] )));
		    
		    node_save($node);
		    
			$date = date_create();
			$timestamp = date_format($date, 'U');
			
		    $user->field_updated = array(LANGUAGE_NONE => array(array('value' => $timestamp)));
		    $save = user_save($user);
		}
		
	}
}
