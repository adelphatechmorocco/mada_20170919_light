<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$d = strtotime(strip_tags($fields['created']->content));
$diff = round(abs(time() - $d) / 60,2);
?>

<tr<?php if($diff>=20){ print " class=\"red\""; }elseif($diff>=10){ print " class=\"orange\""; } ?>" id="s<?php print strip_tags($fields['nid']->content); ?>" <?php if(strip_tags($fields['field_status']->content)!=0 && strip_tags($fields['field_status']->content)!=3){ ?>style="display:none;"<?php } ?>>
      <th scope="row"><?php print $fields['nid']->content; ?></th>
      <td><?php print $fields['field_prenom']->content; ?> <?php print $fields['field_nom']->content; ?></td>
      <td><?php print $fields['mail']->content; ?></td>
      <td><?php print $fields['field_telephone_abn']->content; ?></td>
      <td><?php print $fields['created']->content; ?></td>
      <td>
            <select name="status" nid="<?php print strip_tags($fields['nid']->content); ?>" style="height: inherit; padding: 0 30px 0 10px; font-size: inherit;">
                  <option value="0" <?php if(strip_tags($fields['field_status']->content)==0) print 'selected'; ?>>Nouveau</option>
                  <option value="1" <?php if(strip_tags($fields['field_status']->content)==1) print 'selected'; ?>>Retenu</option>
                  <option value="2" <?php if(strip_tags($fields['field_status']->content)==2) print 'selected'; ?>>Refusé</option>
                  <option value="3" <?php if(strip_tags($fields['field_status']->content)==3) print 'selected'; ?>>A rappeler</option>
                  <option value="4" <?php if(strip_tags($fields['field_status']->content)==4) print 'selected'; ?>>NRP</option>
            </select>
      </td>
</tr>