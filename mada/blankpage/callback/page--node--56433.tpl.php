<?php
global $user;
?>
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Espace de candidature</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
        <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
        <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
        <link rel="manifest" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/callback/manifest.json"/>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/callback/js/gcm.js"></script>
        <script>
        document.addEventListener('DOMContentLoaded', function() {
        //check browser
        var isMobile = false; //initiate as false
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
        if(!isMobile) {
            var isChrome = !!window.chrome && !!window.chrome.webstore;
            if(!isChrome) {
            $('#browser-error').show();
                alert('Attention! Les notifications fonctionnent uniquement sur Chrome.');
            }
        }
        $(function() {
            $('#togglebtn').change(function() {
                console.log('toggle button clicked', $(this).prop('checked'));
                if($(this).prop('checked') === true) {
                    if(Notification.permission !== 'granted') {
                        Notification.requestPermission().then(function(permission) {
                            if(permission === 'granted' && 'serviceWorker' in navigator) {
                                navigator.serviceWorker.register('<?=base_path()?>cb-worker.js').then(initialiseState);
                            } else {
                                console.log('service worker not present');
                            }
                        });
                    }
                } else {
                    unsubscribe();
                    console.log('off notification');
                }
            });

            setInterval(check,15000);

            $('select[name="status"]').change(function() {
                    changeStatus($(this).val(),$(this).attr('nid'));
            });
        });
        //get subscription token if already subscribed
        if(Notification.permission === 'granted') {
            navigator.serviceWorker.ready.then(function(registration) {
                registration.pushManager.getSubscription().then(function(subscription){
                    if(subscription !== null) {
                        getToken(subscription);
                        $('#togglebtn').prop('checked', true).change();
                    } else {
                        $('#togglebtn').prop('checked', false).change();
                    }
                });
            });
        }
        });
        function initialiseState() {
            //check if notification is supported or not
            if(!('showNotification' in ServiceWorkerRegistration.prototype)) {
                console.warn('Notificaiton are not supported');
                return;
            }
            //check if user has blocked push notification
            if(Notification.permission === 'denied'){
                console.warn('User has blocked the notification');
            }
            //check if push messaging is supported or not
            if(!('PushManager' in window)) {
                console.warn('Push messaging is not supported');
                return;
            }
            //subscribe to GCM
            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                //call subscribe method on serviceWorkerRegistration object
                serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})
                .then(function(subscription){
                    getToken(subscription);
                    storeToken(subscription);
                }).catch(function(err){
                    console.error('Error occured while subscribe(): ', err);
                });
            });
        }
        function unsubscribe() {
            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration){
                serviceWorkerRegistration.pushManager.getSubscription().then(function(subscription){
                    subscription.unsubscribe().then(function() {
                    }).catch(function(err){
                        console.error('error while unsubscribe(): ', err);
                    });
                });
            })
        }
        function getToken(subscription) {
            console.log(subscription);
            var token = subscription.endpoint.substring(40, subscription.endpoint.length);
            document.querySelector("#client-token").innerHTML = token;
            //sendNotification();
        }
        function storeToken(subscription) {
            var token = subscription.endpoint.substring(40, subscription.endpoint.length);
            firebase.database().ref('tokens/').push({token: token});
        }
        function check(){
              $.ajax({
               url: '<?=base_path()?>check-callback',
               data: {
                  uid: <?=$user->uid?>,
                  op: 'check'
               },
               dataType: 'text',
               success: function(data) {
                  if(parseInt(data)>0){
                    sendNotification();
                    setTimeout(refresh, 10000);
                  }
               },
               type: 'POST'
            });
        }
        function changeStatus(status,id){
            $.ajax({
                url: '<?=base_path()?>check-callback',
                data: {
                    nid: id,
                    status: status,
                    op: 'edit'
                },
                dataType: 'text',
                success: function(data) {
                    if(parseInt(data)>0){
                        sendNotification();
                    }
                },
                type: 'POST'
            });
        }
        function refresh() {
            window.location.reload(true);
        }
        </script>
        <style>
            .red, .red a {
                color:red !important;
            }
            .orange, .orange a {
                color: darkorange !important;
            }
        </style>
    </head>
    <body>
        <body>
            <div id="wrapper">
                <header class="slide-pages">
                    <div class="top-slide top-slide-candidature" style="background: url('https://recrute.webhelp.ma/sites/default/files/bg-bien-etre_0.png') center 0;">
                        <div class="content-top-slide">
                            <div class="logo-top wow fadeInLeft">
                                <a href="/"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo.png" alt="" class="logo"></a>
                                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/effet-logo.png" alt="" class="fond-logo wow fadeInRight">
                            </div>
                            <nav class="navbar wow fadeInRight">
                                <div class="navbar-header">
                                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-2" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                                </div>
                                <div class="collapse navbar-collapse" id="navbar-collapse-2">
                                    <ul class="nav navbar-nav espace-membre">
                                        <?php if ( $user->uid ) { ?>
                                        <li><a href="<?=base_path()?>user/logout" class="logged_in"><span class="icon-member icon"></span> Se déconnecter</a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="titre">
                            <h1 class="text-center">Web Call Back</h1>
                        </div>
                    </div>
                </header>
                <div id="content">
                    <div class="responsive-main-menu">
                        <div id="o-wrapper" class="o-wrapper responsive-main-menu">
                            <button type="button" data-toggle="collapse" id="c-button--slide-right" data-target="#navbar-collapse-2" class="c-button navbar-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        <nav id="c-menu--slide-right" class="c-menu c-menu--slide-right">
                            <button class="c-menu__close">Close Menu &rarr;</button>
                            <ul class="nav navbar-nav espace-membre">
                                <?php if ( $user->uid ) { ?>
                                <li><a href="<?=base_path()?>user/logout" class="logged_in"><span class="icon-member icon"></span> Se déconnecter</a></li>
                                <?php } ?>
                            </ul>
                        </nav>
                        <div id="c-mask" class="c-mask"></div>
                    </div>
                    <div class="espace-candidature postuler" style="padding-bottom: 150px;">
                        <div class="container-espace-candidature">
                            <div class="alert alert-danger fade in" id="browser-error" style="display: none">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <strong>Attention!</strong> Les notifications fonctionnent uniquement sur Chrome.<span id="gcm-error-msg"></span>
                            </div>
                            <h2>Demandes de Web Call Back</h2>
                            <label>Activer les notification &nbsp;&nbsp;&nbsp;</label>
                            <input id="togglebtn" type="checkbox" data-toggle="toggle" data-on="Oui" data-off="Non">
                            <section>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="table-responsive table-bordered" style="display: none">
                                                <input type="text" id="endpoint" class="form-control" value="https://android.googleapis.com/gcm/send" disabled/>
                                                <input type="text" id="server-key" class="form-control" value="AIzaSyDIK76LEISzxYmKZUhzmmSq9cNrMUDHRjM" />
                                                <textarea id="client-token" rows="6" cols="40"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!--div class="alert alert-success fade in" id="gcm-success" style="display: none">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <strong>Success!</strong> Notification sent.
                            </div-->
                            <div class="alert alert-danger fade in" id="gcm-error" style="display: none">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <strong>Failure!</strong> <span id="gcm-error-msg"></span>
                            </div>
                            <?php // region slider
                                echo views_embed_view('liste_des_demandes_de_callback', 'block');
                            ?>
                        </section>
                    </div>
                </div>
            </div>
            <footer id="footer">
                <div class="footer-bottom footer-bottom-pages">
                    <div class="content-footer-bottom">
                        <p class="text-center">Ce site est conforme à la loi 09-08 relative à la protection des personnes physiques à l´égard du traitement des données à caractère personnel Autorisation
                        de la CNDP (Commission Nationale de Contrôle de la Protection des Données à Caractère Personnel) n : D-W-259/2014</p>
                        <p class="copyright text-center">© 2016 Webhelp, All Rights Reserved. Proudly designed and coded in Morocco <a href="/node/85">Conditions d'utilisations</a></p>
                    </div>
                </div>
            </footer>
        </div>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
        <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
        <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootbox.min.js"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script src="https://www.gstatic.com/firebasejs/3.5.2/firebase.js"></script>
        <script>
        // Initialize Firebase
        var config = {
        apiKey: "AIzaSyBdN2in1ZdOSFFpFXtgYgQ-sO3gQWOXFVY",
        authDomain: "webhelp-callback-bf18e.firebaseapp.com",
        databaseURL: "https://webhelp-callback-bf18e.firebaseio.com",
        storageBucket: "webhelp-callback-bf18e.appspot.com",
        messagingSenderId: "188366722218"
        };
        firebase.initializeApp(config);
        </script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
    </body>
</html>