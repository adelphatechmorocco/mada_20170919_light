
self.addEventListener('push', function(event) {
  console.log('Callback');
  var notificationTitle = 'Nouvelle demande';
  const notificationOptions = {
    body: 'Demande de callback reçue',
    icon: 'https://recrute.webhelp.ma/sites/all/themes/webhelpv2/images/logo.png',
    tag: 'demande-callback',
    data: {
      url: 'http://recrute.webhelp.ma/callback'
    }
  };

  event.waitUntil(
    Promise.all([
      self.registration.showNotification(
        notificationTitle, notificationOptions)
    ])
  );
});

self.addEventListener('notificationclick', function(event) {
  event.notification.close();

  var clickResponsePromise = Promise.resolve();
  if (event.notification.data && event.notification.data.url) {
    clickResponsePromise = clients.openWindow(event.notification.data.url);
  }

  event.waitUntil(
    Promise.all([
      clickResponsePromise
    ])
  );
});