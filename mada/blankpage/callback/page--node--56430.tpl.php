<?php
$q = new EntityFieldQuery;
$q->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'callback')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_status', 'value', array(0,3), 'IN')
    ->fieldCondition('field_telephone_abn', 'value', $_REQUEST['phone']);
date('Y-m-d H:i:s', strtotime($stop_date . ' + 1 day'));
$r = $q->execute();

if(count($results['node'])<=0) {

    $usersUpdated = $field_gsm = array();
    $date = date_create();
    $timestamp = date_format($date, 'U');
    $roleId = 7;

    //Select users role 7
    $query = 'SELECT ur.uid
        FROM {users_roles} AS ur
        WHERE ur.rid = :rid';
    $result = db_query($query, array(':rid' => $roleId));
    $uids = $result->fetchCol();
    $usrs = "";
    foreach ($uids as $u) {
        $user_data = user_load($u);
        $region = $user_data->field_region['und']['0']['value'];
        if($region==$_REQUEST["region"])
            $usrs .= $u . ',';
    }

    //Select user to update
    $query = 'SELECT uid, name FROM wh_users AS u INNER JOIN wh_field_data_field_updated AS up ON u.uid = up.entity_id WHERE u.uid IN (' . rtrim($usrs, ',') . ') ORDER BY up.field_updated_value ASC';

    $result = db_query($query);

    foreach ($result as $k => $v) {
        $usersUpdated[] = $v->uid;
    }

    if (!empty($usersUpdated)) {
        $user = user_load($usersUpdated[0]);
        //Prepare node
        $node = new stdClass();
        $node->title = date("d-m-Y H:i:s") . " | " . $usersUpdated[0] . " | " . $_REQUEST['field_gsm'];
        $node->language = LANGUAGE_NONE;
        $node->type = 'callback';
        $node->status = 1;
        $node->uid = $usersUpdated[0];
        node_object_prepare($node);
        $node->field_uid_reference = array(LANGUAGE_NONE => array(array('uid' => (int)$usersUpdated[0])));
    if (!empty($_REQUEST['uid'])) {
            $acc = user_load($_REQUEST['uid']);
            $edit = array(
                'field_tel' => array(
                    LANGUAGE_NONE => array(
                        0 => array(
                            'value' => $_REQUEST['phone']
                        )
                    )
                ),
                'field_region' => array(
                    LANGUAGE_NONE => array(
                        0 => array(
                            'value' => $_REQUEST['region']
                        )
                    )
                ),
            );

            user_save($acc, $edit);

            $node->field_utilisateur = array(LANGUAGE_NONE => array(array('uid' => $_REQUEST['uid'])));
        } else {
            if (user_load_by_mail($_REQUEST['email'])) {
                print '2';
                die();
            } else {
                $newUser = array(
                    'name' => $_REQUEST['email'],
                    'mail' => $_REQUEST['email'],
                    'pass' => $_REQUEST['password'],
                    'status' => 1,
                    'field_prenom' => array(
                        LANGUAGE_NONE => array(
                            0 => array(
                                'value' => $_REQUEST['prenom']
                            )
                        )
                    ),
                    'field_nom' => array(
                        LANGUAGE_NONE => array(
                            0 => array(
                                'value' => $_REQUEST['nom']
                            )
                        )
                    ),
                    'field_tel' => array(
                        LANGUAGE_NONE => array(
                            0 => array(
                                'value' => $_REQUEST['phone']
                            )
                        )
                    ),
                    'field_region' => array(
                        LANGUAGE_NONE => array(
                            0 => array(
                                'value' => $_REQUEST['region']
                            )
                        )
                    ),
                    'roles' => array(
                        DRUPAL_AUTHENTICATED_RID => 'authenticated user'
                    ),
                );

                $account = user_save(NULL, $newUser);


                $node->field_utilisateur = array(LANGUAGE_NONE => array(array('uid' => $account->uid)));
            }

        }
        $node->field_telephone_abn = array(LANGUAGE_NONE => array(array('value' => $_REQUEST['phone'])));
        $node->field_region = array(LANGUAGE_NONE => array(array('value' => $_REQUEST['region'])));
        node_save($node);
        $user->field_tel = array(LANGUAGE_NONE => array(array('value' => $_REQUEST['phone'])));
        $user->field_updated = array(LANGUAGE_NONE => array(array('value' => $timestamp)));
        $save = user_save($user);
        if ($save)
            print "1";
        else
            print "0";
    }
}