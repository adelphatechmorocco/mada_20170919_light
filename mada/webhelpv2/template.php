<?php

function webhelpv2_form_alter(&$form, &$form_state, $form_id) {

    if ($form_id == 'search_block_form') {
	$form ['actions'] ['submit'] ['#value'] = ''; // Change the text on the submit button
	//$form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');
	unset($form ['actions'] ['submit'] ['#attributes'] ['class']);

	$form ['actions'] ['submit'] ['#attributes'] ['class'] [] = 'input_submit';
	// Add extra attributes to the text box
	$form ['search_block_form']["#default_value"] = t("Recherche");
	$form ['search_block_form'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = 'Recherche';}";
	$form ['search_block_form'] ['#attributes'] ['onfocus'] = "if (this.value == 'Recherche') {this.value = '';}";
	$form ['search_block_form'] ['#attributes']['class'] [] = 'input_text';
	//	$form['actions']['submit']['#value']='';
	//	$form['actions']['submit']['#attributes']['class'][]='Send';
    } elseif ($form_id == 'user_login') {

	$form["name"]['#title'] = t("Identifiant");
	$form["pass"]['#title'] = t("Mot de passe");
	$form["name"]["#description"] = "";
	$form["pass"]["#description"] = "";
	$form['actions']['submit']['#attributes']['class'][] = 'input_submit';
	$form['actions']['submit']['#attributes']["value"] = t("S'authentifier");
	$form ['name'] ['#attributes']['class'] [] = 'input_text';
	$form ['pass'] ['#attributes']['class'] [] = 'input_text';

	$form ['name'] ['#attributes']['value'] [] = 'Login';
	$form ['pass'] ['#attributes']['value'] [] = 'Mot de passe';

	$form ['name'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = 'Login';}";
	$form ['name'] ['#attributes'] ['onfocus'] = "if (this.value == 'Login') {this.value = '';}";

	$form ['pass'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = 'Mot de passe';}";
	$form ['pass'] ['#attributes'] ['onfocus'] = "if (this.value == 'Mot de passe') {this.value = '';}";

	if ($form['#action'] == '/user' or $form['#action'] == '/user/login')
	    $form['#action'] = '/';
    } elseif ($form_id == 'simplenews_block_form_4') {
	$form ['submit'] = array('#type' => 'submit', '#value' => t('OK'));
	$form ['submit'] ['#attributes'] ['class'] [] = 'input_img';
	$form ['mail'] = array('#type' => 'textfield', '#title' => '', '#default_value' => t('Votre mail ici'));
	$form ['mail'] ['#attributes'] ['onblur'] = "if (this.value == '') {this.value = '" . t('Votre mail ici') . "';}";
	$form ['mail'] ['#attributes'] ['onfocus'] = "if (this.value == '" . t('Votre mail ici') . "') {this.value = '';}";
	$form ['mail'] ['#required'] = TRUE;
	$form ['mail'] ['#attributes'] ['class'] [] = 'input_text';
    }
}

function webhelpv2_preprocess_node(&$variables) {

    if ($variables['view_mode'] == 'mini_cv') {
	$variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__mini_cv';
    }

}

function webhelpv2_preprocess_page(&$variables, $hook) {
if ($variables['node']->type == 'offre_d_emploi' || $variables['node']->type == 'actualite'){
       $img_url = 'http://5.79.1.197/mada/sites/all/themes/webhelpv2/css/images/bg-offres-slide.png';

$og_image = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'property' => 'og:image',
    'content' => $img_url,
  ),
);
drupal_add_html_head($og_image, 'og_image');
$og_title = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'property' => 'og:title',
    'content' => $variables['node']->title,
  ),
);
drupal_add_html_head($og_title, 'og_title');
  // echo "<pre>";
  // print_r($variables['node']);
  // echo "</pre>";
    }

    if ($variables['node']->type == 'page'){

   $img_url = file_create_url($variables['node']->field_header_image['und'][0]['uri']);
      $og_image = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'property' => 'og:image',
    'content' => $img_url,
  ),
);
drupal_add_html_head($og_image, 'og_image');
$og_title = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'property' => 'og:title',
    'content' => $variables['node']->title,
  ),
);
drupal_add_html_head($og_title, 'og_title');

  //       echo "<pre>";
  // print_r($variables['node']);
  // echo "</pre>";
    }






    if (isset($variables['node']) && $variables['node']->nid == 118 && !$variables['logged_in']) {
	drupal_goto('user/register');
    } elseif (isset($variables['node']) && $variables['node']->nid == 118 && $variables['logged_in']) {
	drupal_goto('node/add/cv-webhelp');
    }

    $variables['menu_item'] = menu_get_item();
    if ($variables['menu_item']) {
	// Is this a Views page?
	$variables['classes_array'][] = $variables['menu_item']['page_callback'];
    }
    if (isset($variables['node'])) {
	$variables['classes_array'][] = $variables['node']->type;
	if ($variables['node']->type == 'offre_d_emploi')
	    $variables['classes_array'][] = 'evoluer';
    }
    $variables["classes_webhelp"] = "";
    $menuParent = menu_get_active_trail();

    if (!empty($menuParent[1]) && isset($menuParent[1]['link_title'])) {
	$menuParent = $menuParent[1]['link_title'];
	$filter = array('\'' => '', 'é' => 'e', ' ' => '-', '_' => '-', '/' => '-', '[' => '-', ']' => '');
	$variables["classes_webhelp"] = drupal_clean_css_identifier(drupal_strtolower($menuParent), $filter);
    }
	if (isset($variables['node']->type)) {
		$variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
	}
}

//function webhelpv2_menu_link_alter(&$item) {
//
//    //print_r($item);
//    //die;
//}
//
//function webhelpv2_preprocess_menu_link(&$variables) {
//    // the class array is here
//    //print_r(menu_get_item());
//    //print_r($variables['element']);
//    //die;
//}
//
//function webhelpv2_menu_link(array $variables) {
//   // print_r($variables['element']);
//    //die;
//}
function webhelpv2_preprocess_html(&$variables, $hook) {

    if (drupal_is_front_page()) {
	$meta_keywords = array(
	    '#type' => 'html_tag',
	    '#tag' => 'meta',
	    '#attributes' => array(
		'name' => 'keywords',
		'content' => "centre d’appel, call center telemarketing, call center outsourcing, webhelp, outsourcing call center"
	    )
	);
	drupal_add_html_head($meta_description, 'meta_description');
	drupal_add_html_head($meta_keywords, 'meta_keywords');
    }

    /*drupal_add_js($theme_path.'/js/jquery.easing.1.3.js', array('type' => 'file', 'scope' => 'footer'));
    drupal_add_js($theme_path.'js/jquery.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/bootstrap.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/owl.carousel.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/menu.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/wow.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/mouse-check-element.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/waypoints.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/jquery.counterup.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/videoLightning.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/bootstrap-carousel.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/fancybox/jquery.fancybox.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/jquery.bxslider.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/jquery.themepunch.plugins.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/jquery.themepunch.revolution.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/Placeholders.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/select2.full.min.js', array('type' => 'file', 'scope' => 'footer'));
	drupal_add_js($theme_path.'js/main.js', array('type' => 'file', 'scope' => 'footer'));*/
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
function webhelpv2_preprocess_comment(&$variables, $hook) {
    // If comment subjects are disabled, don't display them.

    if (variable_get('comment_subject_field_' . $variables['node']->type, 1) == 0) {
	$variables['title'] = '';
    }
    $uid = $variables["elements"]["#comment"]->uid;
    $utilisateur = user_load($uid);
    $nom = getFieldValue('field_nom', $utilisateur, 'user');
    $prenom = getFieldValue('field_prenom', $utilisateur, 'user');
    // Add pubdate to submitted variable.
    $variables['pubdate'] = '<time pubdate datetime="' . format_date($variables['comment']->created, 'custom', 'c') . '">' . $variables['created'] . '</time>';
    $variables['submitted'] = t('!username replied on !datetime', array('!username' => '<h3 class="author">' . $nom . '  ' . $prenom . '<h3>', '!datetime' => $variables['pubdate']));

    // Zebra striping.
    if ($variables['id'] == 1) {
	$variables['classes_array'][] = 'first';
    }
    if ($variables['id'] == $variables['node']->comment_count) {
	$variables['classes_array'][] = 'last';
    }
    $variables['classes_array'][] = $variables['zebra'];

    $variables['title_attributes_array']['class'][] = 'comment-title';
}

/*function webhelpv2_css_alter(&$css) {
    unset($css [drupal_get_path('module', 'search') . '/search.css']);
}*/

function webhelpv2_webform_mail_headers_3502($form_values, $node, $sid, $cid) {
  $headers = array(
    'Content-Type'  => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'X-Mailer'      => 'Drupal Webform (PHP/'. phpversion() .')',
  );
  return $headers;
}


function webhelpv2_theme() {
  return array(
    'cv_webhelp_node_form' => array(
      'arguments' => array('form' => NULL),
      'template' => 'templates/cv-webhelp-node-form',
      'render element' => 'form'
    ),
  );
}
