<?php
/**
 * @file jcarousel-view.tpl.php
 * View template to display a list as a carousel.
 */
?>
    <?php 
    foreach ($rows as $id => $row): ?>

<div class="list_carousel jcarousel" >   
      <h3><?php print $title; ?></h3>
      
            <?php echo"<ul id='foo".++$id."'>";?> 
            <?php print $row; ?>
        </ul>
        <div class="clearfix"></div>
    <a id="prev<?php print $id;?>" class="prev3" href="#"><img alt="" title="" src="sites/all/themes/webhelp/images/fleche_droite.png" /></a>
    <a id="next<?php print $id;?>" class="next3" href="#"><img alt="" title="" src="sites/all/themes/webhelp/images/fleche_gauche.png" /></a>

</div>
    <?php  endforeach; ?>
