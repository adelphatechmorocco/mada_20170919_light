<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>

<?php

  foreach ($row->field_field_lien_youtube as $video)
  {
        
  	$nom = $video['rendered']['file']["#file"]->filename;
  	$url = $video['rendered']['file']["#file"]->uri;
  	$url_temp=@split(":", $url);
  	if($url_temp[0]=="youtube")
  	{
  		$url_v=str_replace("//v/", "", $url_temp[1]);
  		$url_img="http://img.youtube.com/vi/" . $url_v . "/1.jpg";
  	}else 
  	{
  		$url_img="/sites/all/modules/media/images/icons/default/video-x-generic.png";
  	}
  	echo "<li><a href='".file_create_url($url)."' title=\"$nom\" rel=\"lightvideo[width:500px;height:400px;]\" >
  			<img alt='$nom' src='$url_img' width='120' height='90' />
  		  </a></li>";
 
  }
  ?>