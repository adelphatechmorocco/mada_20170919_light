<?php
    global $base_url;
	$term = taxonomy_term_load($node->field_categorie_actualite[LANGUAGE_NONE][0][tid]);
	$name = $term->name;	
?>
<header class="slide-pages">
    <div class="top-slide top-slide-actualites">
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <h1 class="text-center">Actualités</h1>
            <!-- <h2 class="text-center" style="color: #fff; font-size: 45px"><?=$name?></h3> -->
        </div>
    </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>

<div class="content-page"> 
	<div class=" top-page-content">
	     <?php 
	         print $messages; 
	         if($user->uid && in_array('administrator', $user->roles))
			 	print "<a href='".$base_url."/node/".$node->nid."/edit?destination=node/".$node->nid."' style='float:right; clear:both'>Editer</a>";
	     ?>
		<div id="node-body">
			 <?php 
             print "<h1>".$node->title."</h1>"; 
			 print render($node->body[LANGUAGE_NONE][0][value]); ?>
            <div class="retour"><a href="<?=base_path();?>actualites">Retour à la liste</a></div>
		</div>
	</div>
</div>

<?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>


  <?php print render($page['content']['metatags']); ?>