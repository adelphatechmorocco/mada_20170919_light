<?php
global $user;
if ( $user->uid ) {
  drupal_goto('mada/cv-webhelp');
}
?>
<header class="slide-pages">
<div class="top-slide top-slide-candidature">
    <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
    <div class="titre">
        <h1 class="text-center">Espace de candidature </h1>
        <h2 class="text-center">Postulez en renseignant le formulaire ci-après</h2>
    </div>
</div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
<div class="espace-candidature identif">
	
    <div class="container-espace-candidature">
	    <?php print $messages; ?>
        <?php print drupal_render(drupal_get_form('user_pass')); ?><br /><br /><br />
    </div>
</div>