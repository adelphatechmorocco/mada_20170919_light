<?php
global $user;
if ( $user->uid ) {
  drupal_goto('cv-webhelp');
}
?>
<header class="slide-pages">
<div class="top-slide top-slide-candidature">
    <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
    <div class="titre">
        <h1 class="text-center">Espace de membre </h1>
        <h2 class="text-center">Postulez en renseignant le formulaire ci-après</h2>
    </div>
</div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
<div class="espace-candidature identif">
    <div class="container-espace-candidature">
	    <?php print $messages; ?>
        <h3 class="text-center">Bienvenue !</h3>
        <div class="contenu-form">
            <div class="identif-page">
                <div class="identif-left">
                    <div class="connecte-toi">
                        <p>Connectez-vous
                            <br/> avec votre réseau social</p>
                       
                        <ul class="social-media">
                            <li><a href="<?=base_path()?>user/simple-fb-connect"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?=base_path()?>linkedin/login/0"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<!--                             <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="ou">
                    - ou -
                </div>
                <div class="identif-right">
                    <div class="espace-membre-identif">
                        <p>Espace membre</p>
                        <form action="<?=base_path();?>/?q=user" class="form-signin" method="post" id="user-login" accept-charset="UTF-8">
                            <input type="text" class="form-control" name="name" placeholder="Identifiant" required="" />
                            <input type="password" class="form-control" name="pass" placeholder="Mot de passe" required="" />
                            <input type="hidden" name="form_build_id" value="form-1l6PtUZBDfn01DA1yeq8qf6yHk9vxHbPpABnKOE-d3k">
                            <input type="hidden" name="form_id" value="user_login">
                            <input type="hidden" name="lastcookie" value="">
                            <button class="btn btn-lg btn-block" type="submit">Connexion</button>
                        </form>
                        <div class="bottom-espace-membre">
                            <a href="<?=base_path();?>user/register">Créer un compte</a>
                            <a href="<?=base_path();?>user/password" class="pwd-oublie">Mot de passe oublié ?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>