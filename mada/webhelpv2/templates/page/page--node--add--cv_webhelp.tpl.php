<?php 
module_load_include('inc', 'node', 'node.pages');
$form = node_add('cv_webhelp');

?>
<header class="slide-pages">
<div class="top-slide top-slide-candidature">
    <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
    <div class="titre">
        <h1 class="text-center">Espace de candidature </h1>
        <h2 class="text-center">Postulez en renseignant le formulaire ci-après</h2>
    </div>
</div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>
<div id="content">
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
<div class="espace-candidature">
    <div class="container-espace-candidature">
    <?php print $messages; ?>
            <div class="container-espace-candidature edition btns">
					<?php print render($page['content']); ?>
	        </div>
            <div class="buttons">
                <button type="submit" class="postuler btn">postuler</button>
                <!--button type="submit" class="btn wow postuler-rappel">postuler Avec rappel</button-->
            </div>
        <div class="entretien-video">
            <a href="#"><p class="text-center">Présentez vous et passez votre entretien en vidéo</p></a>
        </div>
    </div>
</div>