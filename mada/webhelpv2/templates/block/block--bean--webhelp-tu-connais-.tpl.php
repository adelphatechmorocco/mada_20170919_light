<?php 
$el = $elements['bean']['webhelp-tu-connais-']['#entity'];
?>
<div class="webhelp-multinationale  who-we-are qui-sommes-nous <?php print $classes; ?>" <?php print $attributes; ?> id="webhelp">
    <?php
    print render($title_suffix);
    ?>
    <div class="content-webhelp-multinationale">
        <div class="text-webhelp-multinationale text-center">
            <h2 class="text-center wow zoomIn"><?php print $el->title; ?></h2>
            <?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']; ?>
        </div>
        <div class="webhelp-map">
            <div class="items-webhelp-map france-map wow zoomIn">
                <div class="map">
                    <a href="<?php print url($el->field_lien_bouton_pays[LANGUAGE_NONE]['0']['value']); ?>">
                        <img alt="" src="<?=file_create_url($el->field_carte_du_pays[LANGUAGE_NONE]['0']['uri'])?>" class="icons">
                    </a>
                    <!--div class="marker marker1">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker2">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker3">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker4">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker5">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker6">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker7">
                        <div class='pin'></div>
                    </div>
                    <div class="marker marker8">
                        <div class='pin'></div>
                    </div-->
                </div>
                <h4><?php print $el->field_nom_du_pays[LANGUAGE_NONE]['0']['value']; ?></h4>
                <?php print $el->field_description_pays[LANGUAGE_NONE]['0']['value']; ?>
                <a class="link" href="<?php print url($el->field_lien_bouton_pays[LANGUAGE_NONE]['0']['value']); ?>"><?php print $el->field_titre_bouton_pays[LANGUAGE_NONE]['0']['value']; ?></a>
            </div>
            <div class="items-webhelp-map maroc-icon wow fadeInUp">
                <i class="map-marker"></i>
                <br/>
                <div id="text-carousel">
                    <!-- Wrapper for slides --> 
                    <?php 
                    $pays = explode(',',$el->field_liste_des_pays[LANGUAGE_NONE]['0']['value']);
                    ?>
                    <ul>
                    <?php foreach ($pays as $p): ?>
                        <li class="item active">
                            <span><?=$p?></span>
                        </li>
                    <?php endforeach; ?>
                    </ul> 
                </div>
            </div>
            <div class="items-webhelp-map multinatinal-map wow zoomIn"  data-wow-duration="2s">
                <div class="map">
                    <a target="_blank" href="<?php print url($el->field_lien_bouton_monde[LANGUAGE_NONE]['0']['value']); ?>">
                        <img alt="" src="<?=file_create_url($el->field_carte_du_monde[LANGUAGE_NONE]['0']['uri'])?>" class="img-responsive">
                    </a>
                </div>
                <h4><?php print $el->field_titre_carte_monde[LANGUAGE_NONE]['0']['value']; ?></h4>
                <?php print $el->field_description_carte_monde[LANGUAGE_NONE]['0']['value']; ?>
                <a class="link" target="_blank" href="<?php print $el->field_lien_bouton_monde[LANGUAGE_NONE]['0']['value']; ?>"><?php print $el->field_titre_bouton_monde[LANGUAGE_NONE]['0']['value']; ?></a>
            </div>
        </div>
    </div>
</div>