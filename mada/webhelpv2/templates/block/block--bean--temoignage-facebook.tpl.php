<?php 
$el = $elements['bean']['temoignage-facebook']['#entity'];
?>

<div class="avis-collaborateurs avis-collaborateurs-pays <?php print $classes; ?>" <?php print $attributes; ?>">
<?php
print render($title_suffix);
?>
    <div class="container">
        <div class="content-avis wow zoomIn">
            <p><a href="#">@<?php print $el->field_nom[LANGUAGE_NONE]['0']['value']; ?></a>
            <?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']; ?></p>
        </div>
    </div>
</div>