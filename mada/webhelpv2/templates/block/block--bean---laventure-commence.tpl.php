<?php 
$el = $elements['bean']['-laventure-commence']['#entity'];
?>
<!--*********************************************************l'aventure commence**********************************************************-->
<div class="aventure-commence aventure-portugal aventure-france <?php print $classes; ?>" id="aventure" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <div class="content-aventure-commence text-center">
            <div class="left-aventure">
                <h2 class="wow zoomIn text-center"><?=$el->title?></h2>
                <div class="content-recrutement wow zoomIn">
                    <span class="number text-center">1</span>
                    <p class="text-number"><a href="<?php print $el->field_lien[LANGUAGE_NONE]['0']['value']; ?>" style="color:inherit !important"><?php print $el->field_nom[LANGUAGE_NONE]['0']['value']; ?></a></p>
                </div>
                <div class="icon-suite wow fadeInUp">
                    <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/icon1.png">
                    <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/icon2.png">
                    <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/icon3.png">
                </div>
                <div class="content-integration wow zoomIn"  data-wow-duration="2s">
                    <span class="number text-center">2</span>
                    <p class="text-number"><a href="<?php print $el->field_lien_2[LANGUAGE_NONE]['0']['value']; ?>" style="color:inherit !important"><?php print $el->field_titre_2[LANGUAGE_NONE]['0']['value']; ?></a></p>
                </div>
            </div>
        </div>
    </div>
</div>