<?php 
$el = $elements['bean']['ton-avenir-avec-nous']['#entity'];
$el2 = $elements['bean']['ton-avenir-avec-nous']['field_etapes'];
$n = count($el2['#items']);
?> 
<style>
	.content-avenir-avec-nous .box-content-item a,.content-avenir-avec-nous .box-content-item a:hover{
		color: inherit !important;
	}
</style>
<div class="avenir-avec-nous working-with-us avenir-france <?php print $classes; ?>" <?php print $attributes; ?> id="avenir" style="z-index: 99999">
    <?php
    print render($title_suffix);
    ?>
    <div class="content-avenir-avec-nous">
        <h2 class="text-center wow zoomIn"><?php print $el->title; ?></h2>
        <div class="text-center metiers wow zoomIn"><?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']; ?></div>
        <div class="meties-120">
            <a class="tanbtn" href="<?php print url($el->field_lien_du_bouton[LANGUAGE_NONE]['0']['value']); ?>"><?php print $el->field_titre_du_bouton[LANGUAGE_NONE]['0']['value']; ?> </a>
        </div>
        <div class="text-center parcours wow zoomIn"  data-wow-duration="2s"> <?php print $el->field_autre[LANGUAGE_NONE]['0']['value']; ?>
        </div>
        <div class="box-avantages-webhelp">
            <?php 
            for($i=0; $i<$n; $i++):
            $index = $el2['#items'][$i]['value'];
            $pon=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']); 
            $poff=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']); 
            if($i%2==0){
                $c="fadeInUp";
            }else{
                $c="fadeInDown";
            }
            ?>
            <div class="box-content-item wow zoomIn">
	            <a href="<?=url($el2[$i]['entity']['field_collection_item'][$index]['field_lien']['#items'][0]['value'])?>">
                <div class="box-content">
                    <div class="imgs-cherche imgs-box cf">
                        <img alt="" class="top" src="<?=$poff?>">
                        <img alt="" class="bottom" src="<?=$pon?>">
                    </div>
                    <div class="box-text">
                        <h3><?=$el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></h3>
                        <h6><?=$el2[$i]['entity']['field_collection_item'][$index]['field_titre_2']['#items'][0]['value']?></h6>
                        <p><?=$el2[$i]['entity']['field_collection_item'][$index]['field_description_courte']['#items'][0]['value']?></p>
                    </div>
                </div>
	            </a>
            </div>
            <?php endfor; ?>
        </div>
    </div>
</div>