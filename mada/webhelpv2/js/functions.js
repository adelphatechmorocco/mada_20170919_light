/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery.noConflict();

jQuery(document).ready(function() {
    console.log('FANCYBOX');
    jQuery('.fancybox').fancybox({
        title : 'Réservation de créneau',
        afterClose  : function() {
        	location.href = "/";
         }
    });     
    jQuery('.creneau .messages.status').html('Ta candidature a bien été enregistrée. Tu peux maintenant choisir le créneau qui te convient : nos chargés de recrutement t\'appelleront à ce moment pour un entretien rapide.');     
})