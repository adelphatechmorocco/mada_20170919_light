var styles =  [{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"color":"#000000"},{"hue":"#000000"},{"saturation":100},{"lightness":100},{"gamma":0.01}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#000000"},{"hue":"#752b75"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#0092e0"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"},{"invert_lightness":false},{"color":"#c6e376"},{"weight":0.1},{"gamma":1.03}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"color":"#4a4a4a"}]},{"featureType":"road","elementType":"all","stylers":[{"color":"#dae5eb"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f5fcff"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"},{"invert_lightness":false}]}];

var locations = [
    ['meknes', 33.8935200, -5.5472700],
    ['agadir', 30.4201800, -9.5981500],
    ['fes', 34.0371500, -4.9998000],
    ['rabat', 34.0132500, -6.8325500],
    ['kenitra', 34.2610100, -6.5802000]
];
var map,
    markers = [];

function callMap(markers, id) {
    var mapID = (id) ? id : 'map',
        num_markers = locations,
        lat = 31.2,
        lng = -10,
        r = [];

    if(markers){
        r = markers.split(",");
        //num_markers = new Array(r);

        lat = num_markers[0][1];       
        lng = num_markers[0][2];       
    }

    map = new google.maps.Map(document.getElementById(mapID), {
        zoom: ($(window).width()<1440) ? 6 : 7,
        center: new google.maps.LatLng(lat,lng), //31.79170, -7.09262
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: styles,
        disableDefaultUI: true,
        zoomControl: true
    });
     
    if(( typeof markers) !=  undefined)
        addAllMarkers(num_markers,r);
    else
        addAllMarkers();

}

function addAllMarkers(num_markers,r){  
    for (var i = 0; i < num_markers.length; i++) {

        markers[i] = new google.maps.Marker({
            position: { lat: parseFloat(num_markers[i][1]), lng: parseFloat(num_markers[i][2]) },
            map: map,
            html: num_markers[i][0],
            id: i,
            icon: {
                url: (r[0] == num_markers[i][0]) ? "http://"+location.host+"/sites/all/themes/blankpage/images/orange-marker.png" : "http://"+location.host+"/sites/all/themes/blankpage/images/blue-marker.png",
            },

        }); 

        var infowindow = new google.maps.InfoWindow({
            content: num_markers[i][0]
        });


        google.maps.event.addListener(markers[i], 'click', function() {  
            for (var i = 0; i < num_markers.length; i++) {
                markers[i].setIcon("http://"+location.host+"/sites/all/themes/blankpage/images/blue-marker.png");
            }

            var _this = this;
                _this.setIcon("http://"+location.host+"/sites/all/themes/blankpage/images/orange-marker.png");
            
            var ville = this.html;

            $(".ville").fadeOut("slow", function(){
               setTimeout(function(){
                $("#"+ville).fadeIn();
               },500); 
            });


            google.maps.event.addListenerOnce(infowindow, 'closeclick', function() {
                markers[this.id].setVisible(true);
            }); 
        });
    }
}

callMap();

$(window).resize(function(){
    callMap();
});
